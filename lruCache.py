#!/usr/bin/env python
# coding: utf-8

# In[14]:





# In[87]:


class Node:
    '''
        Node class for the LRU cache. The LRU cache consists of a double-linked list(cache) of these nodes. 
        Each node has a key and a value as well as links to next and previous nodes for faster access.
    '''
    def __init__(self,key,value):
        self.key = key
        self.value = value
        self.next = None
        self.prev = None
        self.cached = False
        ##cached is True if it exists in the doubled linked list
class LRUCache:
    '''
     The LRU cache consists of a double-linked list(cache) and a dictionary. The dictionary has a map of 
     keys and Nodes which contain values for the keys as well as next and prev pointers for the cache.
    '''
    def __init__(self,size):
        self.size = size
        self.dict = {}
        self.head = None
        self.tail = None
        self.__cache_count = 0 #private variable
    def show_cache(self):
        ## display keys of the cache in decorated format
        self.displayCache()
    def put(self,key,value):
        '''
        Add element to the LRU Cache.
        If key exists, value is updated and brought to front of the queue(DLL tail)
        '''
        if self.dict.get(key)!=None:
            self.dict[key].value = value
            ##calling get will update position to front of the queue
            self.get(key)
        else:
            n = Node(key,value)
            self.dict[key] = n
            self.add_to_cache(n)
    def get(self,key):
        '''
            Get element for a certain key.
            The latest accessed element goes to the tail of the doubly linked list.
            The latest accessed elements are at the end of the linked list.
            The element is first deleted from the cache(not from the dictionary) and then readded to the tail
            Get: O(1)
        '''
        print("Accessing key:",key)
        if self.dict.get(key)==None:
            print("Key "+str(key)+" does not exist")
            return 0
        ##delete from cache if it exists
        #print(self.dict[key])
        if self.dict[key].cached==True:
            self.delete_from_cache(key)
        
        self.add_to_cache(self.dict[key])
        self.displayCache()
        
        return self.dict[key].value
    def delete(self,key):
        '''
        errors out if element does not exist.
        deletes from cache first and then the dictionary
        '''
        if key not in self.dict:
            print("Key "+str(key) + "does not exist")
            return 0
        else:
            print("Key "+str(key) +" deleted")
            self.delete_from_cache(key)
            self.dict[key] = None
    def add_to_cache(self,n):
        '''
        adds new element to the cache.
        Function used by the put function.
        Removes the least recently used element(at the head of the doubly linked list) when capacity exceeds.
        '''
        if self.__cache_count > self.size-1:
            self.delete_from_cache(self.head.key)
        self.__cache_count = self.__cache_count + 1
        if self.head==None:
            self.head = n
            self.tail = n
            self.head.next = None
            self.tail.next = None
        else:
            temp = self.tail
            self.tail.next = n
            self.tail = n
            self.tail.prev = temp
            self.tail.next=None
        #print("Adding to cache: ",self.tail.key)
        n.cached = True
    def delete_from_cache(self,key):
        '''
        Deletes node by key from the doubly linked list(cache).
        '''
        n = self.dict[key]
        prev = n.prev
        nxt = n.next
        if prev!=None:
            prev.next = nxt
        else:
            self.head = nxt
        if nxt!=None:
            nxt.prev = prev
        else:
            self.tail = prev
        self.__cache_count = self.__cache_count - 1
        n.cached = False
    def displayCache(self):
        curr = self.head
        count = 0
        key_string = ""
        value_string = ""
        if curr==None:
            print("Empty cache")
            return 0
        while curr!=None:
            link_string = "->" if (curr.next!=None) else ""
            key_string = key_string + str(curr.key) + link_string
            value_string = value_string + str(curr.value) + "->" + link_string
            curr = curr.next
        print("LRU Cache keys: ",key_string)
    def reset(self):
        ##reset cache by removing the next and prev links of the DLL nodes
        print("Resetting cache")
        curr = self.head
        while curr!=None:
            tmp = curr.next
            curr.next = None
            curr.prev = None
            curr.cached = False
            curr = tmp
        self.head = None
        self.tail = None
        self.__cache_count = 0


# In[95]:

def main():
	if __name__== "__main__" :
		lruCache = LRUCache(5)
		lruCache.put(3,4)
		lruCache.show_cache()
		lruCache.put(2,5)
		lruCache.show_cache()
		lruCache.put(3,5)
		lruCache.show_cache()
		lruCache.put(14,5)
		lruCache.show_cache()
		lruCache.get(2)
		lruCache.show_cache()
		lruCache.put(13,5)

		lruCache.reset()


		lruCache.show_cache()
		lruCache.put(19,5)
		lruCache.show_cache()
		lruCache.put(21,5)
		lruCache.show_cache()
		lruCache.get(14)
		lruCache.show_cache()
		lruCache.get(100)
		lruCache.show_cache()
		lruCache.put(100,23)
		lruCache.show_cache()
		lruCache.get(19)
		lruCache.show_cache()
		lruCache.delete(14)
		lruCache.show_cache()
		lruCache.get(101)
		lruCache.show_cache()
		lruCache.put(102,2)
		lruCache.show_cache()
		lruCache.put(103,3)
		lruCache.show_cache()
		lruCache.put(104,2)
		lruCache.show_cache()
		lruCache.put(105,3)
		lruCache.show_cache()
		lruCache.get(21)
		lruCache.show_cache()
		lruCache.get(100)
		lruCache.show_cache()
main()




# In[70]:





# In[ ]:





# In[ ]:





# In[ ]:




